package com.example.myapplication6

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer

import com.example.myapplication6.client.APIRequester
import com.example.myapplication6.data.database.CitiesDao
import com.example.myapplication6.data.models.MovieRequest
import com.example.myapplication6.data.repository.CitiesRepository
import com.example.myapplication6.displays.forecast.summary.CitiesForecastSummaryViewModel
import com.example.myapplication6.util.ParsingConstants

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

import java.util.ArrayList

import org.junit.Assert.assertEquals
import org.mockito.Mockito.`when`


class CitiesForecastSummaryViewModelTester {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private var citiesRepository: CitiesRepository? = null

    private var citiesForecastSummaryViewModel: CitiesForecastSummaryViewModel? = null

    @Mock
    private val apiRequester: APIRequester? = null

    @Mock
    private val citiesDao: CitiesDao? = null

    @Before
    fun prepareViewModelParameters() {
        MockitoAnnotations.initMocks(this)
        citiesRepository = CitiesRepository(citiesDao!!)
        citiesForecastSummaryViewModel = CitiesForecastSummaryViewModel(apiRequester!!, citiesRepository!!)
    }

    @Test
    fun ObservingRepositoryMovieRequestLiveDataFromViewModelTest1() {

        //GIVEN
        val REQUEST_NAME = "lord"
        val EXPECTED_MOVIE_NAME = "Lord of War"
        val movieRequestList = ArrayList<MovieRequest>()
        movieRequestList.add(MovieRequest(EXPECTED_MOVIE_NAME))
        `when`(citiesDao!![ParsingConstants.formatLikeQuery(REQUEST_NAME)]).thenReturn(movieRequestList)
        val observer = MyObserver(EXPECTED_MOVIE_NAME)
        citiesForecastSummaryViewModel!!.cityListMutableLiveData.observeForever(observer)
        //WHEN
        citiesForecastSummaryViewModel!!.loadMoviesNameFromDB(REQUEST_NAME)
        //THEN
        Mockito.verify(citiesDao, Mockito.times(1))[ParsingConstants.formatLikeQuery(REQUEST_NAME)]
    }

    private class MyObserver(private val movieName: String) : Observer<List<MovieRequest>> {

        override fun onChanged(movieRequests: List<MovieRequest>) {
            println(movieName)
            val movieRequest = movieRequests[0]
            println(movieRequest.name)
            //THEN
            assertEquals(movieName, movieRequest.name)
        }
    }
}