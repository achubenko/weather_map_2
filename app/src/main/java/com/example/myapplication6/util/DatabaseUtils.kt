package com.example.myapplication6.util

import android.content.res.AssetManager
import android.util.Log
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.lang.StringBuilder

class DatabaseUtils {
    companion object{

        val TAG = DatabaseUtils::class.java.simpleName

        fun getSqlString(assets: AssetManager):String?{
            val reader: BufferedReader
            val sb = StringBuilder()
            try{
                reader = BufferedReader(InputStreamReader(assets.open("databases/cities_creation_query.txt")))
                reader.lines().forEach{sb.append(it)}

            } catch (e: Exception){

            }
            val result = sb.toString()
            Log.d(TAG, "aChub getSqlString() sql string read")
            return if(result.isNotEmpty()){ result }else{null}
        }
    }
}