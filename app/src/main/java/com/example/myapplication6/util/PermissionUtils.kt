package com.example.myapplication6.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

class PermissionUtils {
    companion object{

        val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        val PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2
        val MULTIPLE_PERMISSIONS_REQUEST = 3

        fun hasPermission(context: Context, permission: String): Boolean
                = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED

        fun checkLocationPermissions(activity: Activity):Boolean{
            var answer = hasLocationPermissions(activity)
            if(!answer){
                activity.requestPermissions(arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION), PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
                answer = hasLocationPermissions(activity)
            }
            return answer
        }

        fun checkWritePermissions(activity: Activity):Boolean{
            var answer = hasPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            if(!answer){
                activity.requestPermissions(arrayOf<String>(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE)
                answer = hasPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            return answer
        }



        fun hasLocationPermissions(activity: Activity):Boolean{
            return (                    (hasPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION))
                    and (hasPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)))
        }
    }
}