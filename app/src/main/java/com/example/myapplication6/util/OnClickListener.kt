package com.example.myapplication6.util

import android.os.Bundle

interface OnClickListener {
    fun openMapFragment(bundle: Bundle, backStackTag: String)
}
