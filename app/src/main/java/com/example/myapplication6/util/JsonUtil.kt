package com.example.myapplication6.util

import com.example.myapplication6.data.models.City
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.Stream

class JsonUtil {

    companion object{

        val BATCH_SIZE = 5_000

        fun getCitiesParallelStreamFromFile(file : File): Stream<City> {
            val inputStream = FileInputStream(file)

            val reader = JsonReader(InputStreamReader( inputStream, "UTF-8"))

            reader.beginArray()

            val cityListFromJson = object : List<City>{

                var countedSize = -1

                override val size: Int = 0
//                get() = if(countedSize> -1){
//                    countedSize
//                }else{
//                    var tempSize = 0
//                    while (reader.hasNext()){
//                        tempSize++
//                    }
//                    countedSize = tempSize
//                    countedSize
//                }

                override fun contains(element: City) = false
                override fun containsAll(elements: Collection<City>) = false

                override fun get(index: Int): City {
                    return City()
                }

                override fun indexOf(element: City)=0

                override fun isEmpty() = false

                override fun iterator(): Iterator<City> {
                    return object : Iterator<City>{
                        override fun hasNext(): Boolean {
                            return reader.hasNext()
                        }

                        override fun next(): City {
                            val city = Gson().fromJson<City>(reader, City::class.java)
                            return city
                        }
                    }
                }

                override fun lastIndexOf(element: City) = 0
                override fun listIterator(): ListIterator<City> {
                    return listIterator
                }

                val listIterator = object : ListIterator<City>{

                    var nextIndexCount: AtomicInteger = AtomicInteger(0)

                    override fun hasNext(): Boolean {
                        return reader.hasNext()
                    }

                    override fun hasPrevious() = false

                    override fun next(): City {
                        val city = Gson().fromJson<City>(reader, City::class.java)
                        nextIndexCount.set(nextIndexCount.get()+1)
                        return city
                    }

                    override fun nextIndex(): Int {
                        return nextIndexCount.get() +1
                    }

                    override fun previous(): City {
                        return City()
                    }

                    override fun previousIndex(): Int {
                        return nextIndexCount.get() -1
                    }

                }


                override fun listIterator(index: Int): ListIterator<City> {
                    return listIterator
                }

                override fun subList(fromIndex: Int, toIndex: Int): List<City> {
                    return ArrayList<City>()
                }
            }

            return cityListFromJson.parallelStream()

        }
    }

}