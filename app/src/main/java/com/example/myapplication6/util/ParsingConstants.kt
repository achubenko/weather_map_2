package com.example.myapplication6.util

class ParsingConstants {
    companion object{

        val APP_SETS = "app_sets"

        val OFFLINE = "offline"

        val CITY_ID = "omdbID"

        fun formatLikeQuery(name: String): String {
            return "$name%"
        }
    }
}
