package com.example.myapplication6.util

import android.location.Location

class LocationUtils{
    companion object {
        /**
         * This method does not take in account that lat and lng could be belonged to the different hemisphere
         */
        fun getHaversineDistance(lat1: Double, lng1: Double, lat2: Double, lng2: Double): Double {
            val a = (lat1 - lat2) * distPerLat(lat1)
            val b = (lng1 - lng2) * distPerLng(lat1)
            return Math.sqrt(a * a + b * b)
        }

        private fun distPerLng(lat: Double): Double {
            return (0.0003121092 * Math.pow(lat, 4.0) + 0.0101182384 * Math.pow(lat, 3.0) - 17.2385140059 * lat * lat
                    + 5.5485277537 * lat + 111301.967182595)
        }

        private fun distPerLat(lat: Double): Double {
            return -0.000000487305676 * Math.pow(lat, 4.0) - 0.0033668574 * Math.pow(lat, 3.0) + 0.4601181791 * lat * lat - 1.4558127346 * lat + 110579.25662316
        }

        fun distance(loc1 : Location, loc2: Location) = loc1.distanceTo(loc2)
    }
}