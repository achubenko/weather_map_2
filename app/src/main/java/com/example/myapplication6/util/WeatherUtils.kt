package com.example.myapplication6.util

import com.example.myapplication6.data.models.WeatherPersistence

class WeatherUtils {
    companion object{
        val TAG = WeatherUtils::class.java.simpleName

        fun getActualWeatherItem(list: List<WeatherPersistence>, curTime: Long = System.currentTimeMillis()):WeatherPersistence
                = list.find{ it.date>curTime }?:list.last()

        fun getActualWeather6HourlyItems(list: List<WeatherPersistence>, curTime: Long = System.currentTimeMillis()):List<WeatherPersistence>
                = list.filter{ it.date>curTime }.take(6).ifEmpty { list.takeLast(6)}

        fun toCel(kelvin: Double) = (kelvin-273.15).toInt()

    }
}