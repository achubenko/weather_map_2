package com.example.myapplication6.displays.forecast.summary

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.location.LocationListener
import android.os.Build
import com.example.myapplication6.R

import android.os.Bundle

import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import javax.inject.Inject
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.core.view.MenuItemCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import com.example.myapplication6.data.models.*
import com.example.myapplication6.displays.forecast.summary.geocoder.interactor.GeocoderResultCallback
import com.example.myapplication6.displays.forecast.summary.geocoder.interactor.GeocoderServiceInteractionDelegate
import com.example.myapplication6.displays.forecast.summary.viewpager.CityPagerAdapter
import com.example.myapplication6.services.FetchAddressIntentService
import com.example.myapplication6.util.PermissionUtils.Companion.MULTIPLE_PERMISSIONS_REQUEST
import com.example.myapplication6.util.PermissionUtils.Companion.PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
import com.google.android.gms.location.*
import dagger.android.support.AndroidSupportInjection

class CitiesForecastSummaryFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener, GeocoderResultCallback {

    //    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var searchView: SearchView? = null

    private var movieTextQuery: String? = null
    //todo should be initailized before adapters
    private lateinit var mCitiesForecastSummaryViewModel: CitiesForecastSummaryViewModel
    private var cityPagerAdapter: CityPagerAdapter? = null
    private var searchCityListRecyclerAdapter: SearchCityListRecyclerAdapter? = null
    private var searchMenuItem: MenuItem? = null
    private var cityForecastViewPager: ViewPager? = null
    private var rootView: View? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    @set:Inject
    var viewModelFactory: ViewModelProvider.Factory? = null
    //todo provide with Dagger
    val geocoderInteractionDelegate = GeocoderServiceInteractionDelegate(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        checkRequiredPermissions()
        initProperties()
    }

    fun initProperties(){
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    // Update UI with location data
                    // ...
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_summary, container, false)
        setHasOptionsMenu(true)
//        checkRequiredPermissions()
        //todo keep this experimental
        tuneViewModel()
        initViewPager()
//        tuneSwipeRefreshLayout(rootView!!)
        return rootView
    }

    private fun checkRequiredPermissions(){
        if (checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)!=PERMISSION_GRANTED
                &&
                checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)!=PERMISSION_GRANTED
                &&
                checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PERMISSION_GRANTED

        ){
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,  Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE), MULTIPLE_PERMISSIONS_REQUEST)
        }
    }

    private fun tuneViewModel() {
        mCitiesForecastSummaryViewModel = ViewModelProviders
                .of(this, viewModelFactory).get(CitiesForecastSummaryViewModel::class.java)

        startObservation()
        provideDeviceLocationRetrieving()
    }

    //todo think about how to sent FusedLocationProviderClient to ViewModelProvider with the Dagger
    private fun startObservation(){
        //todo keep this experimental
        mCitiesForecastSummaryViewModel.cityNodesListLiveData.observe(this,  Observer<List<CityNode>> {
            Log.d(TAG, "aChub mCitiesForecastSummaryViewModel.cityNodesListLiveData.observe")
            it.forEach{Log.d(TAG, it.city.name)}
            renewViewPager(it)
        })
        mCitiesForecastSummaryViewModel.errorMutableLiveData.observe(this, Observer<Error> { this.actOnFailure(it) })
        mCitiesForecastSummaryViewModel.searchedCityPersistanceListLiveData.observe(this, Observer<List<CityPersistance>> {
            Log.d(TAG, "aChub mCitiesForecastSummaryViewModel.searchedCityPersistanceListLiveData.observe")
            this.updateDropDownList(it)
        })
    }

    private fun provideDeviceLocationRetrieving(){
        if (checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)!=PERMISSION_GRANTED){
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }

        else{
            tuneDeviceLocationSettings()
        }
    }

    private fun tuneDeviceLocationSettings(){
        //todo spread logic for more build versions
        if(Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission( context!!, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission( context!!, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        val locationManager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val locationListener: LocationListener = object: LocationListener {
            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
            override fun onProviderEnabled(provider: String?) {}
            override fun onProviderDisabled(provider: String?) {}
            override fun onLocationChanged(location: Location?) {

                Log.d(TAG, "aChub onLocationChanged()")
                location?.let {
                    locationManager.removeUpdates(this)
                    geocoderInteractionDelegate.startIntentService(it)}
            }
        }
//        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)




//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000L,
//                10F, locationListener, activity!!.mainLooper)
        locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, locationListener, null)


//        fusedLocationClient.lastLocation.addOnSuccessListener {
//            Log.d(TAG, "aChub OnSuccessListener")
//            locationManager.removeUpdates(locationListener)
//            geocoderInteractionDelegate.startIntentService(it!!)
//        }

//        fusedLocationClient.lastLocation.addOnFailureListener {
//            Log.d(TAG, "aChub OnFailureListener")
//            fusedLocationClient.requestLocationUpdates(LocationRequest(), locationCallback, activity!!.mainLooper) }


    }

    override fun getIntentForClass(className: Class<out Any>) = Intent(context!!, className)

    override fun startService(intent: Intent) {
//        fusedLocationClient.removeLocationUpdates(locationCallback)
//        Log.d(TAG, "aChub startService")
        FetchAddressIntentService.enqueueWork(context!!, intent)
//        context!!.startService(intent)
    }

    override fun processExpectedAddress(location: Location, address: Address) {
        mCitiesForecastSummaryViewModel.processDeviceLocation(location, address)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                if((permissions.indices).filter { grantResults[it]==PackageManager.PERMISSION_GRANTED }.size==permissions.size){
                    tuneDeviceLocationSettings()
                }
                //todo show toast
            }
            PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
                if((permissions.indices).filter { grantResults[it]==PackageManager.PERMISSION_GRANTED }.size==permissions.size){
//                    populateCitiesTable()
                }
                //todo show toast
            }

            MULTIPLE_PERMISSIONS_REQUEST -> {
                if((permissions.indices).filter { grantResults[it]==PackageManager.PERMISSION_GRANTED }.size==permissions.size){
                    tuneDeviceLocationSettings()
//                    populateCitiesTable()
                }
                //todo show toast
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        searchMenuItem = menu.findItem(R.id.action_search)
        if (searchMenuItem != null) {
            searchView = MenuItemCompat.getActionView(searchMenuItem!!) as SearchView
            searchView!!.setOnQueryTextListener(this)
            searchView!!.queryHint = "Search"
        }
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null && savedInstanceState.containsKey(QUERY_KEY)) {
            movieTextQuery = savedInstanceState.getString(QUERY_KEY)
            savedInstanceState.remove(QUERY_KEY)
        }
        initDropDownCityListRecyclerView(rootView!!)
        mCitiesForecastSummaryViewModel.loadTrackedCities()
    }

    private fun initViewPager(){
        cityForecastViewPager = rootView!!.findViewById(R.id.city_forecast_view_pager) as ViewPager
        cityPagerAdapter = CityPagerAdapter(childFragmentManager)
        cityForecastViewPager!!.adapter = cityPagerAdapter
        cityForecastViewPager!!.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {

            var oldFragment: Fragment? = null
            var currentFragment: Fragment? = null

            override fun onPageSelected(position: Int) {
                if(oldFragment!=currentFragment){
                    oldFragment = currentFragment
                    oldFragment?.onPause()
                    currentFragment = (cityForecastViewPager!!.adapter as CityPagerAdapter).registeredFragments[position]
                    currentFragment?.onResume()
                }
            }
        })
    }

    private fun renewViewPager(cityNodesList: List<CityNode>) {
        val list: MutableList<CityNode> = ArrayList()
        list.addAll(cityNodesList)
        cityPagerAdapter!!.setData(list)
        cityForecastViewPager!!.refreshDrawableState()
        stopRefreshing()
    }

    fun updateDropDownList(cityList: List<CityPersistance>) {
        searchCityListRecyclerAdapter!!.cityList = cityList
    }

    private fun initDropDownCityListRecyclerView(view: View) {
        searchCityListRecyclerAdapter = SearchCityListRecyclerAdapter(mCitiesForecastSummaryViewModel)
        val cityListRecycleView = view.findViewById<RecyclerView>(R.id.cities_list_recycler_view)
        val linearLayoutManager = LinearLayoutManager(context)
        cityListRecycleView.layoutManager = linearLayoutManager
        cityListRecycleView.adapter = searchCityListRecyclerAdapter
    }

    override fun onQueryTextSubmit(newText: String?): Boolean {
        if (newText == null || newText.trim { it <= ' ' }.isEmpty()) {
            return false
        }
//        mCitiesForecastSummaryViewModel.trackCity(newText)
        movieTextQuery = newText
        if (searchMenuItem != null) {
            searchMenuItem!!.collapseActionView()
        }
        searchCityListRecyclerAdapter!!.clear()
//        mCitiesForecastSummaryViewModel.loadCitiesList()

        return false
    }

    override fun onQueryTextChange(newText: String): Boolean {
        return if(newText.isEmpty()){
            searchCityListRecyclerAdapter!!.clear()
            false
        }
        else{
            mCitiesForecastSummaryViewModel.loadCitiesByName(newText)
            false
        }
    }

    fun actOnFailure(error: Error) {
        stopRefreshing()
        Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show()
    }

    private fun tuneSwipeRefreshLayout(inflatedView: View) {
//        swipeRefreshLayout = inflatedView.findViewById<View>(R.id.swipe_refresh_layout) as SwipeRefreshLayout
//        swipeRefreshLayout!!.setOnRefreshListener(this)
    }

    private fun stopRefreshing() {
//        if (swipeRefreshLayout != null && swipeRefreshLayout!!.isRefreshing) {
//            swipeRefreshLayout!!.isRefreshing = false
//        }
    }

    override fun onRefresh() {
        if (movieTextQuery == null || movieTextQuery!!.trim { it <= ' ' }.isEmpty()) {
            stopRefreshing()
            Toast.makeText(context, "Please enter film name", Toast.LENGTH_SHORT).show()
        } else {
//            mCitiesForecastSummaryViewModel.loadCitiesList()
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        if (!(movieTextQuery == null || movieTextQuery!!.trim { it <= ' ' }.isEmpty())) {
//            mCitiesForecastSummaryViewModel.loadCitiesList()
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (!(movieTextQuery == null || movieTextQuery!!.trim { it <= ' ' }.isEmpty())) {
            outState.putString(QUERY_KEY, movieTextQuery)
        }
    }

    companion object {
        private val QUERY_KEY = "movieTextQuery"
        private val TAG = CitiesForecastSummaryFragment::class.java.simpleName
        private val REQUEST_LOCATION = 532
        private val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
    }
}
