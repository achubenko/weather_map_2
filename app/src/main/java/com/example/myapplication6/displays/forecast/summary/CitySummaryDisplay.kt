package com.example.myapplication6.displays.forecast.summary
import com.example.myapplication6.data.models.CityPersistance

interface CitySummaryDisplay {

    interface CityTrackingUpdater {
        fun updateCityPersistence(cityPersistance: CityPersistance)
    }

    interface ContractPresenter: CityTrackingUpdater {

    }


}
