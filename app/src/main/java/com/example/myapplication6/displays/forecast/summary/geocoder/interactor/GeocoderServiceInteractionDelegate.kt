package com.example.myapplication6.displays.forecast.summary.geocoder.interactor

import android.location.Address
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.util.Log
import com.example.myapplication6.services.FetchAddressIntentService

class GeocoderServiceInteractionDelegate(val geocoderResultCallback: GeocoderResultCallback) : GeocoderInteractionDelegate, Handler(){

    private var resultReceiver: AddressResultReceiver = AddressResultReceiver(this)

    override fun startIntentService(location: Location) {

        val intent = geocoderResultCallback.getIntentForClass(FetchAddressIntentService::class.java).apply {
            putExtra(FetchAddressIntentService.RECEIVER, resultReceiver)
            putExtra(FetchAddressIntentService.LOCATION_DATA_EXTRA, location)
        }
        geocoderResultCallback.startService(intent)
    }

    internal inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            Log.d(TAG, "aChub onReceiveResult(...)")
            if (resultCode == FetchAddressIntentService.SUCCESS_RESULT) {
                val address = resultData!!.getParcelable<Address>(FetchAddressIntentService.RESULT_DATA_KEY)!!
                val location = resultData!!.getParcelable<Location>(FetchAddressIntentService.LOCATION_DATA_EXTRA)!!
                geocoderResultCallback
                        .processExpectedAddress(location, address)
            }
        }
    }

    companion object{
        val TAG = GeocoderServiceInteractionDelegate::class.java.simpleName
    }
}