package com.example.myapplication6.displays.forecast.summary

import android.location.Address
import android.location.Location
import com.example.myapplication6.data.repository.CitiesRepository

import android.util.Log

import javax.inject.Inject

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication6.data.models.CityNode
import com.example.myapplication6.data.models.CityPersistance
import com.example.myapplication6.data.repository.WeatherRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import kotlin.collections.HashMap

class CitiesForecastSummaryViewModel @Inject
constructor(private val cityRepository: CitiesRepository, private val weatherRepository: WeatherRepository) : ViewModel(), CitySummaryDisplay.ContractPresenter {

    val errorMutableLiveData: MutableLiveData<Error>
    val cityNodesListLiveData: MutableLiveData<List<CityNode>>
    val searchedCityPersistanceListLiveData: MutableLiveData<List<CityPersistance>>
    val availableCitiesDiposable = CompositeDisposable()

    private var currentLocation: Location? = null
    private var trackedCities: MutableMap<Long, CityPersistance> =  HashMap<Long, CityPersistance>()
    private var disposable: CompositeDisposable = CompositeDisposable()

    init {
        errorMutableLiveData = MutableLiveData()
        cityNodesListLiveData = MutableLiveData()
        searchedCityPersistanceListLiveData = MutableLiveData()
    }

    override fun updateCityPersistence(cityPersistance: CityPersistance) {
        cityRepository.updateCity(cityPersistance)
        loadTrackedCities()
    }

    fun processDeviceLocation(curLocation: Location?, address: Address){
        currentLocation = curLocation
        findCurrentCityWeatherPersistance(curLocation, address)
    }

    private fun findCurrentCityWeatherPersistance(curLocation: Location?, address: Address){
                disposable += cityRepository.getCitiesByCountry(address.countryCode)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { if(it!=null && curLocation!=null)
                                    {

                                        it.minBy { c->curLocation.distanceTo(Location("").apply{latitude = c.latitude; longitude = c.longitude}) }?.let{
                                            Log.d(TAG, "aChub findCurrentCityWeatherPersistance it.name: ${it.name}")
                                        trackedCities.putIfAbsent(it.id, it)}
                                        trackedCities.values.forEach{Log.d(TAG, (it.name))}
                                    }
                                loadCityNodes()
                                }, {
                                    throwable -> Log.e(TAG, "aChub Throwable1 " + (throwable as java.lang.Error).message)
                                }
                        )
    }

    fun loadCitiesByName(name: String) {
        if(name.length<4) return
        var count = 0
        var count2 = 0
        availableCitiesDiposable.clear()
        availableCitiesDiposable += cityRepository.getCitiesByName(name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    count2++
                    it.forEach {
                        count++
                        if(count==17575){
                            Log.d(TAG, "aChub endpoint reached")
                        }
                    }
                    searchedCityPersistanceListLiveData.value = it
                },{throwable -> Log.e(TAG, "aChub Throwable1 " + (throwable as java.lang.Error).message)})
    }

    fun loadTrackedCities(){
        cityRepository.getTrackedCities().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                it.forEach{n-> trackedCities[n.id] = n
                        //todo think about adding cached data
                    loadCityNodes()
                    }
                },{throwable -> Log.e(TAG, "aChub Throwable1 " + (throwable as java.lang.Error).message)})
    }

    var loadCityNodesCount = 0

    fun loadCityNodes(){
        loadCityNodesCount ++
        Log.d(TAG, "aChub loadCityNodes() loadCityNodesCount : $loadCityNodesCount")
        if(trackedCities.isNotEmpty()){
            Log.d(TAG, "aChub loadCityNodes() if")
            Log.d(TAG, "aChub loadCityNodes() trackedCities.values.toList().size : ${trackedCities.values.toList().size}")
            //todo remove disposable and check
            disposable.clear()

            disposable = CompositeDisposable()

        disposable +=
                weatherRepository.getObservableCityNodeList(trackedCities.values.toList())
                .observeOn(AndroidSchedulers.mainThread())
                        //todo replace logic between threads correctly
                .subscribeOn(Schedulers.io())
                        .subscribe ( {
                            Log.d(TAG, "aChub answer ${it.size}")
                            cityNodesListLiveData.value = it
                        },{Log.d(TAG, "aChub loadCityNodes() onError() ${it.message}")}
//                .subscribeWith(object : DisposableObserver<List<CityNode>>(){
//                    override fun onComplete() {
//                        Log.d(TAG, "aChub loadCityNodes() onComplete()")
//                    }
//
//                    override fun onNext(t: List<CityNode>) {
//                        cityNodesListLiveData.value = t
//                        Log.d(TAG, "aChub loadCityNodes() onNext() 2; weatherlist.size : ${t.size}")
//                    }
//                    override fun onError(e: Throwable) {
//                        Log.d(TAG, "aChub loadCityNodes() onError()")
//                    }
//
//                }

                )
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
        availableCitiesDiposable.dispose()
    }

    companion object {

        private val TAG = CitiesForecastSummaryViewModel::class.java.simpleName
    }
}
