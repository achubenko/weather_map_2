package com.example.myapplication6.displays.forecast.summary.geocoder.interactor

import android.content.Intent
import android.location.Address
import android.location.Location

interface GeocoderResultCallback {

    //todo keep methods for optimization

    fun getIntentForClass(className:  Class<out Any>): Intent

    fun startService(intent: Intent)

    fun processExpectedAddress(location: Location, address: Address)
}