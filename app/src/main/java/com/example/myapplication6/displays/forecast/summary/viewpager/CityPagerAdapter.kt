package com.example.myapplication6.displays.forecast.summary.viewpager


import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.example.myapplication6.R
import com.example.myapplication6.data.models.CityNode
import com.example.myapplication6.displays.forecast.map.CityMapFragment
import com.example.myapplication6.displays.forecast.summary.viewpager.CityPageFragment.Companion.newInstance
import com.example.myapplication6.util.OnClickListener


class CityPagerAdapter(val fm: FragmentManager) : FragmentStatePagerAdapter(fm), OnClickListener {

    private var data: List<CityNode> = ArrayList()

    var registeredFragments: SparseArray<CityPageFragment> = SparseArray()

    init{

    }

    override fun getItem(position: Int): Fragment {
        val fragment = newInstance(data[position], position)
        registeredFragments.put(position, fragment)

        return fragment
    }
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        Log.d(TAG, "aChub instantiateItem(); position : ${position}")
        val fragment = newInstance(data[position], position)
//        container.addView(fragment.view)
        registeredFragments.put(position, fragment)
        fm.beginTransaction().add(container.id, fragment).commit()
        return fragment
    }

    fun getRegisteredFragment(position: Int): Fragment {
        Log.d(TAG, "aChub getRegisteredFragment(); position : ${position}")
        return registeredFragments.get(position)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        Log.d(TAG, "aChub destroyItem(); position : ${position}")
//        super.destroyItem(container, position, `object`)

        fm.beginTransaction().remove(`object` as CityPageFragment).commit()

        registeredFragments.remove(position)
    }

    fun setData(data: List<CityNode>){
        Log.d(TAG, "aChub2 setData() ${data.size}")
        this.data = data
//        renewRegisteredFragments(data)
        notifyDataSetChanged()
    }

//    private fun renewRegisteredFragments(data: List<CityNode>){
//        registeredFragments = (data.indices).map{CityPageFragment.newInstance(data[it], it)}.toList()
//    }


//        }

    override fun getCount(): Int {
        Log.d(TAG, "aChub2 getCount() data.size: ${data.size}")
        return data.size}

    override fun getItemPosition(`object`: Any): Int {
        Log.d(TAG, "aChub2 getItemPosition() ${(`object`as CityPageFragment).arguments!!.getInt("position")
        }}")
//        val fPosition = (`object` as Fragment).arguments!!.getInt("position")
//        val fCityNode = (`object` as Fragment).arguments!!.getSerializable("CITY_NODE")
//        val indexInList = data.indexOf(fCityNode)
//        if(indexInList <0 || indexInList != fPosition )
//            return PagerAdapter.POSITION_UNCHANGED
//
//
        (`object`as CityPageFragment)?.let { it.arguments!!.getInt("position")}?: PagerAdapter.POSITION_NONE

        return (`object`as CityPageFragment).arguments!!.getInt("position")
    }



//    override fun isViewFromObject(view: View, `object`: Any) = view == `object`

    override fun openMapFragment(bundle: Bundle, backStackTag: String) {
        Log.d(TAG, "openMapFragment")
        val detailFragment = CityMapFragment()
        detailFragment.arguments = bundle
        //todo replace with childFragmentManager
        fm.beginTransaction()
                .replace(R.id.fragment_container, detailFragment)
                .addToBackStack(backStackTag).commit()
    }


    companion object {
        private val TAG = CityPagerAdapter::class.java.simpleName
    }


}



