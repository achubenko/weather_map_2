package com.example.myapplication6.displays.forecast.map


import android.Manifest
import android.content.Context.LOCATION_SERVICE
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication6.data.models.WeatherPersistence
import com.example.myapplication6.util.ParsingConstants.Companion.CITY_ID
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import androidx.core.app.ActivityCompat
import com.example.myapplication6.R
import com.example.myapplication6.data.models.CityNode
import com.example.myapplication6.displays.forecast.summary.viewpager.CityPageFragment
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions


class CityMapFragment : Fragment(), OnMapReadyCallback, LocationListener {
    @set:Inject
    var viewModelFactory: ViewModelProvider.Factory? = null

    private var cityId: Long? = null

    private var mapFragmentViewModel: MapFragmentViewModel? = null
    private var fragmentView: View? = null
    private var mapView: MapView? = null
    private var cardVIew: CardView? = null
    private var myLocationTextView: TextView? = null
    private var lastUpdatedTextView: TextView? = null
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private var mLocationManager: LocationManager? = null
    private var mLocationPermissionGranted = false

    lateinit var cityNode: CityNode

    lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            cityId = arguments!!.getLong(CITY_ID)
            Log.d(TAG, "cityId: " + cityId!!)
        } else if (savedInstanceState != null) {
            cityId = savedInstanceState.getLong(CITY_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_map, container, false)
        mapView = fragmentView!!.run { findViewById(R.id.map_view) }
        initiateMapView(savedInstanceState)
        cardVIew = fragmentView!!.findViewById((R.id.card_view))
        myLocationTextView = fragmentView!!.findViewById(R.id.my_location_text_view)
        lastUpdatedTextView = fragmentView!!.findViewById(R.id.last_updated_text_view)


        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context!!)
        mLocationManager = context!!.getSystemService(LOCATION_SERVICE) as LocationManager
        return fragmentView
    }

    private fun initiateMapView(savedInstanceState: Bundle?){
        mapView!!.onCreate(savedInstanceState)
        mapView!!.onResume()
        MapsInitializer.initialize(activity!!.applicationContext)
        mapView!!.getMapAsync(this)

    }

    private fun tuneLocationUpdates() {
        if (checkPermission()) {
            mLocationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000L,
                    10F, this, activity!!.mainLooper)
        } else {
            //todo move on Pause
            mLocationManager?.removeUpdates(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tuneLocationUpdates()
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cityNode = arguments!!.getSerializable(CityPageFragment.CITY_NODE) as CityNode
        startObservation()
    }



    private fun startObservation() {
        mapFragmentViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(MapFragmentViewModel::class.java)
        //FIXME remove binding from viewmodel
//        mapFragmentViewModel!!.cityId = cityId!!
//        mapFragmentViewModel!!.initViewModelFields()
//        mapFragmentViewModel!!.weatherPersistenceMutableLiveData!!.observe(this,
//                Observer<List<WeatherPersistence>>{ weatherPersistanceList ->
//                    run {
//                        pointCurrentCity(weatherPersistanceList)
//                    }
//                })
//        mapFragmentViewModel!!.errorMutableLiveData.observe(this, Observer<Error> { this.actOnFailure(it) })
//        mapFragmentViewModel!!.toastMessageMutableLiveData!!.observe(this, Observer<String> { this.showToast(it) })
//        mapFragmentViewModel!!.retrieveData()
    }


    override fun onProviderDisabled(provider: String?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onLocationChanged(location: Location?) {

    }

    private fun pointCurrentCity(weatherPersistenceList: List<WeatherPersistence>){

    }

    private fun actOnFailure(error: Error) {
        showToast(error.message!!)
    }

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    //todo change with CityNode
    override fun onMapReady(googleMap: GoogleMap) {
//        var weatherPersistanceList = mapFragmentViewModel!!.weatherPersistenceMutableLiveData!!.value
//        var weatherItem = weatherPersistanceList?.let{getActualWeatherItem(it)}

//        var pointer = LatLng(weatherItem?.)
        showPicker(googleMap)
    }

    private fun showPicker(googleMap: GoogleMap){
        //todo move to separate util method
//        cityNode.city.latitude
        val latlng = LatLng(cityNode.city.latitude, cityNode.city.longitude)

        mMap = googleMap
        mMap.getUiSettings().setZoomControlsEnabled(true)
        mMap.addMarker(MarkerOptions().position(latlng).title(cityNode.city.name))

        val builder = LatLngBounds.Builder()
        builder.include(latlng)
        val bounds = builder.build()
//        val cu = CameraUpdateFactory.newLatLngBounds(bounds, 12)
        val cu = CameraUpdateFactory.newLatLngZoom(latlng, 10f)
        googleMap.animateCamera(cu)
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onResume() { super.onResume(); mapView?.onResume() }
    override fun onPause() { super.onPause(); mapView?.onPause() }
    override fun onStop() { super.onStop(); mapView?.onStop() }

    fun popBackStack() {
        fragmentManager!!.popBackStack()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(TAG, "onSaveInstanceState(...) invoked")
        outState.putLong(CITY_ID, cityId!!)
    }

    private fun checkPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true
        } else if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = false
            requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)

            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionGranted = true
            }
        }
        return mLocationPermissionGranted
    }


    companion object {
        private val TAG = CityMapFragment::class.java.simpleName
        val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
    }
}
