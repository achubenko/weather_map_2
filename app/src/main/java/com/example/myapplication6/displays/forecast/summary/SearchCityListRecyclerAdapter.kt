package com.example.myapplication6.displays.forecast.summary

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication6.data.models.CityPersistance
import com.example.myapplication6.databinding.CityListItemBinding

class SearchCityListRecyclerAdapter(private val cityTrackingUpdater: CitySummaryDisplay.CityTrackingUpdater) : RecyclerView.Adapter<SearchCityListRecyclerAdapter.CityListViewHolder>() {

    var cityList: List<CityPersistance> = EMPTY_LIST
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CityListViewHolder {
        val layoutInflater = LayoutInflater.from(viewGroup.context)
        val binding = CityListItemBinding.inflate(layoutInflater, viewGroup, false)
        return CityListViewHolder(binding.root)
    }

    override fun onBindViewHolder(cityListViewHolder: CityListViewHolder, i: Int) {
        cityListViewHolder.binding.city = cityList[i]
        cityListViewHolder.cityTrackingUpdater = cityTrackingUpdater
    }

    override fun getItemCount() = cityList.size

    fun clear() {
        cityList = EMPTY_LIST
        notifyDataSetChanged()
    }

    class CityListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var binding: CityListItemBinding = DataBindingUtil.bind(itemView)!!
        internal lateinit var cityTrackingUpdater: CitySummaryDisplay.CityTrackingUpdater

        init {
            binding.holder = this
        }

        fun trackCity() {
            val tempCity = binding.city!!
            tempCity.isTracked = !tempCity.isTracked
            cityTrackingUpdater.updateCityPersistence(tempCity)
            binding.city = tempCity
        }
    }

    companion object{
        private val TAG = SearchCityListRecyclerAdapter::class.java.simpleName
        private val EMPTY_LIST: List<CityPersistance> = ArrayList(0)
    }
}
