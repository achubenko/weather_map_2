package com.example.myapplication6.displays.welcome

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class WelcomeActivity : AppCompatActivity(), HasSupportFragmentInjector {

    //todo move here permission checking, initializing workmanager and worker status displaying

    @set:Inject
    internal var fragmentAndroidInjector: DispatchingAndroidInjector<Fragment>? = null

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return fragmentAndroidInjector
    }

}