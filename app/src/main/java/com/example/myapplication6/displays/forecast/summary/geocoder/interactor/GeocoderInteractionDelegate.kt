package com.example.myapplication6.displays.forecast.summary.geocoder.interactor

import android.location.Location

interface GeocoderInteractionDelegate {
    fun startIntentService(location: Location)
}