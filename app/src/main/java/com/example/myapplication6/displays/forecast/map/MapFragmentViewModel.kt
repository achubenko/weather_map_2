package com.example.myapplication6.displays.forecast.map

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication6.data.models.WeatherPersistence
import com.example.myapplication6.data.repository.WeatherRepository
import com.example.myapplication6.displays.forecast.summary.CitiesForecastSummaryViewModel
import javax.inject.Inject

class MapFragmentViewModel @Inject
constructor(private val repository: WeatherRepository) : ViewModel() {

    private var mDetailsView: MovieDetailsRoomContract.DetailsView? = null
    var weatherPersistenceMutableLiveData: MutableLiveData<List<WeatherPersistence>>? = null
        private set

    var toastMessageMutableLiveData: MutableLiveData<String>? = null
        private set
    var cityId: Long? = null

    val errorMutableLiveData: MutableLiveData<Error>
        get() = repository.errorMutableLiveData



    fun initViewModelFields() {
        toastMessageMutableLiveData = MutableLiveData()
        weatherPersistenceMutableLiveData = repository.weatherListLiveData
    }


    fun setDetailsView(detailsView: MovieDetailsRoomContract.DetailsView) {
        mDetailsView = detailsView
    }

    fun retrieveData() {
//        repository.loadWeatherPersistanceLiveData(this.cityId!!)
    }


    companion object {
        private val TAG = CitiesForecastSummaryViewModel::class.java.simpleName
    }
}
