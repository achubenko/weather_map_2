package com.example.myapplication6.displays.forecast.summary.viewpager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.myapplication6.R
import com.example.myapplication6.data.models.CityNode
import com.example.myapplication6.displays.forecast.map.CityMapFragment
import com.example.myapplication6.ui.HourlyTemperatureGraphCanvasView
import com.example.myapplication6.util.WeatherUtils.Companion.getActualWeatherItem

class CityPageFragment: Fragment(), View.OnClickListener {

    private lateinit var rootView: View
    private lateinit var cityNode: CityNode
    lateinit var hourlyCanvasView: HourlyTemperatureGraphCanvasView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cityNode = arguments!!.getSerializable(CITY_NODE) as CityNode
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_city_card, container, false)
        tuneSingleCityView()
        return rootView
    }

    override fun onClick(v: View?) {
        var mapFragment = CityMapFragment()
        val bundle = Bundle()
        bundle.putSerializable(CITY_NODE, cityNode)
        mapFragment.arguments = bundle
        var transaction = activity!!.supportFragmentManager!!.beginTransaction()
//        transaction.replace(R.id.fragment_container, mapFragment as Fragment).addToBackStack("mapFragment").commit()
        transaction.replace(R.id.fragment_container, mapFragment).addToBackStack("mapFragment").commit()
    }

    //todo change with binding
    //todo cover more weather data
    private fun tuneSingleCityView() {
        val node = cityNode
        val cityNameTV = rootView.findViewById<TextView>(R.id.city_name_text_view)
        cityNameTV.text = cityNode.city.name
        cityNameTV.setOnClickListener(this)
        val cityCurTemp = rootView.findViewById<TextView>(R.id.temperature_text_view)
        hourlyCanvasView = rootView.findViewById<HourlyTemperatureGraphCanvasView>(R.id.hourlyCanvasView)
        hourlyCanvasView.cityNode = cityNode
        cityCurTemp.text = (getActualWeatherItem(node.weatherList!!).temp).toString()+" \u2103"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hourlyCanvasView.cityNode = cityNode
    }

    companion object{
        val TAG: String = CityPageFragment::class.java.simpleName

        const val CITY_NODE = "CITY_NODE"

        fun newInstance(cityNode: CityNode, position: Int): CityPageFragment {
            val fragment = CityPageFragment()
            fragment.arguments = Bundle().apply {
                putSerializable(CITY_NODE, cityNode)
                putInt("position", position)
            }
            return fragment
        }
    }
}