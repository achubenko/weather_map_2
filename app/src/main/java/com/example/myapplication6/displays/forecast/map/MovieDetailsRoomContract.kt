package com.example.myapplication6.displays.forecast.map

import android.app.AlertDialog

import java.io.File

interface MovieDetailsRoomContract {

    interface DetailsView {

        fun getAsyncAlertDialogBuilder(): AlertDialog.Builder
        fun checkWPermission(permissionName: String): Boolean

        fun openWebViewFragment()

        fun popBackStack()

        fun showAlertDialog(message: String)

        fun startBrowserActivity()

        fun getFileFromExternalStorage(fileURI: String): File
    }
}
