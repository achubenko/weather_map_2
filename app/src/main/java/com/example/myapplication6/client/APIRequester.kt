package com.example.myapplication6.client

import com.example.myapplication6.BuildConfig
import com.example.myapplication6.data.models.CityWeatherResponse
import io.reactivex.Single

import retrofit2.http.GET
import retrofit2.http.Query

interface APIRequester {

//    @GET("/")
//    fun getWeather5d3hForCity(@Query("s") searchedMovieTitle: Long): Single<CityWeatherResponse>

    @GET("/data/2.5/forecast")
    fun getWeather5d3hForCity(@Query("id") cityId: Long,
                              @Query("appid") apikey: String = BuildConfig.OWM_API_KEY): Single<CityWeatherResponse>

}
