package com.example.myapplication6.di.builder

import com.example.myapplication6.displays.forecast.map.CityMapFragment
import com.example.myapplication6.displays.forecast.summary.CitiesForecastSummaryFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    internal abstract fun contibuteGridFragment(): CitiesForecastSummaryFragment

    @ContributesAndroidInjector
    internal abstract fun contibuteDetailFragment(): CityMapFragment
}
