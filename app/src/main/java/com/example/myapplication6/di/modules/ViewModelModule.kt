package com.example.myapplication6.di.modules

import com.example.myapplication6.displays.ViewModelFactory
import com.example.myapplication6.displays.forecast.map.MapFragmentViewModel
import com.example.myapplication6.displays.forecast.summary.CitiesForecastSummaryViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication6.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MapFragmentViewModel::class)
    abstract fun bindMovieDetailsViewModel(movieDetailsViewModel: MapFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CitiesForecastSummaryViewModel::class)
    abstract fun bindMovieRequestsViewModel(citiesForecastSummaryViewModel: CitiesForecastSummaryViewModel): ViewModel

    @Binds
    abstract fun bindsViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

}
