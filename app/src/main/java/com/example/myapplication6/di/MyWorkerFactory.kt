package com.example.myapplication6.di

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.example.myapplication6.data.database.CitiesDao
import com.example.myapplication6.workers.CityListPopulateDbWorker
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MyWorkerFactory

@Inject constructor(val citiesDao: CitiesDao): WorkerFactory() {

    var areWorkersNeeded = citiesDao.allList().isEmpty()

    override fun createWorker(appContext: Context, workerClassName: String, workerParameters: WorkerParameters): ListenableWorker? {
        return if(areWorkersNeeded) {
            val workerKlass = Class.forName(workerClassName).asSubclass(Worker::class.java)
            val constructor = workerKlass.getDeclaredConstructor(Context::class.java, WorkerParameters::class.java)
            val instance = constructor.newInstance(appContext, workerParameters)

            when (instance) {
                is CityListPopulateDbWorker -> (instance as CityListPopulateDbWorker).citiesDao = citiesDao
            }

             instance

        }else {
             null
        }
    }
}