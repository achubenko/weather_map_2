package com.example.myapplication6.di.modules

import androidx.work.ListenableWorker
import androidx.work.WorkerFactory
import com.example.myapplication6.di.MyWorkerFactory
import com.example.myapplication6.di.WorkerKey
import com.example.myapplication6.workers.CityListPopulateDbWorker
import com.example.myapplication6.workers.DeleteJsonFileDbWorker
import com.example.myapplication6.workers.DownloadJsonFileWorker
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class WorkerFactoryModule2{
    @Binds
    @IntoMap
    @WorkerKey(DownloadJsonFileWorker::class)
    abstract fun bindDownlodJsonFileWorker(downloadJsonFileWorker: DownloadJsonFileWorker): ListenableWorker

    @Binds
    @IntoMap
    @WorkerKey(CityListPopulateDbWorker::class)
    abstract fun bindCityListPopulateDbWorker(cityListPopulateDbWorker: CityListPopulateDbWorker): ListenableWorker

//    @Binds
//    @IntoMap
//    @WorkerKey(CityListPopulateDbWorker::class)
//    abstract fun bindCityListPopulateDbWorker(cityListPopulateDbWorker: CityListPopulateDbWorker.Factory): Worker

    @Binds
    @IntoMap
    @WorkerKey(DeleteJsonFileDbWorker::class)
    abstract fun bindDeleteJsonFileDbWorker(deleteJsonFileDbWorker: DeleteJsonFileDbWorker): ListenableWorker

    @Binds
    abstract fun bindsMyWorkerFactory(myWorkerFactory: MyWorkerFactory): WorkerFactory


}


