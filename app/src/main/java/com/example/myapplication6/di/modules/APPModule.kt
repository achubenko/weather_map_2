package com.example.myapplication6.di.modules


import android.app.Application
import android.util.Log

import com.example.myapplication6.client.APIRequester
import com.example.myapplication6.data.database.AppDatabase
import com.example.myapplication6.data.database.CitiesDao

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
@Module(includes = [ViewModelModule::class, WorkerFactoryModule2::class, DbModule::class])
class APPModule {

    companion object {
        private val TAG = APPModule::class.java.simpleName
        private val BASE_URL = "https://openweathermap.org/"
    }

    @Provides
    @Singleton
    fun getHttpLoggingInterceptor(): HttpLoggingInterceptor {
        Log.d(TAG, "getHttpLoggingInterceptor()")
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @Singleton
    fun getApiRequester(retrofit: Retrofit): APIRequester {
        Log.d(TAG, "getApiRequester()")
        return retrofit.create(APIRequester::class.java)
    }

    @Provides
    @Singleton
    fun getRetrofit(okHttpClient: OkHttpClient): Retrofit {
        Log.d(TAG, "getRetrofit()")
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun getOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        Log.d(TAG, "getOkHttpClient()")
        return OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor).build()
    }

}
