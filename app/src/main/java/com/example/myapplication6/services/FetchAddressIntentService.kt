package com.example.myapplication6.services

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.ResultReceiver
import android.util.Log
import androidx.core.app.JobIntentService
import java.io.IOException
import java.util.*
import androidx.core.content.ContextCompat.getSystemService



class FetchAddressIntentService : JobIntentService(){

    //todo check class of the ResultReceiver in different APIs
    private var receiver: ResultReceiver? = null

    private var location: Location? = null



    private fun deliverResultToReceiver(resultCode: Int, address: Address?) {
        Log.d(TAG, "aChub deliverResultToReceiver receiver==null: ${receiver==null}")
        val bundle = Bundle().apply {
            putParcelable(RESULT_DATA_KEY, address)
            putParcelable(LOCATION_DATA_EXTRA, location!!)
        }
        receiver?.send(resultCode, bundle)
    }

    override fun onHandleWork(intent: Intent) {
        Log.d(TAG, "aChub onHandleWork(...)")
        intent ?: return
        val geocoder = Geocoder(this, Locale.getDefault())

        var errorMessage = ""

        // Get the location passed to this service through an extra.
        location = intent.getParcelableExtra<Location>(
                LOCATION_DATA_EXTRA)

        receiver = intent.getParcelableExtra(FetchAddressIntentService.RECEIVER)

        var addresses: List<Address> = emptyList()

        try {
            addresses = geocoder.getFromLocation(
                    location!!.latitude,
                    location!!.longitude,
                    // In this sample, we get just a single address.
                    1)
        } catch (ioException: IOException) {
            // Catch network or other I/O problems.
            errorMessage = "service_not_available"
            Log.e(TAG, errorMessage, ioException)
        } catch (illegalArgumentException: IllegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = "invalid_lat_long_used"
            Log.e(TAG, "$errorMessage. Latitude = $location.latitude , " +
                    "Longitude =  $location.longitude", illegalArgumentException)
        }

        // Handle case where no address was found.
        if (addresses.isEmpty()) {
            if (errorMessage.isEmpty()) {
                errorMessage = "no_address_found"
                Log.e(TAG, errorMessage)
            }
            deliverResultToReceiver(FAILURE_RESULT, null)
        } else {
            val address = addresses[0]
            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.



            val addressFragments = with(address) {
                (0..maxAddressLineIndex).map { getAddressLine(it) }
            }
            Log.i(TAG, "address_found")
            deliverResultToReceiver(SUCCESS_RESULT,
                    address)
        }


    }

    companion object{
         private val TAG = FetchAddressIntentService::class.java.simpleName
        const val SUCCESS_RESULT = 0
        const val FAILURE_RESULT = 1
        const val PACKAGE_NAME = "com.example.myapplication6"
        const val RECEIVER = "$PACKAGE_NAME.RECEIVER"
        const val RESULT_DATA_KEY = "${PACKAGE_NAME}.RESULT_DATA_KEY"
        const val LOCATION_DATA_EXTRA = "${PACKAGE_NAME}.LOCATION_DATA_EXTRA"
        private const val JOB_ID = 1000

        fun enqueueWork(context: Context, work: Intent) {
            enqueueWork(context, FetchAddressIntentService::class.java, JOB_ID, work)
        }
    }
}