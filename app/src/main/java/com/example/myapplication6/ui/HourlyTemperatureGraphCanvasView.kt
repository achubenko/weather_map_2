package com.example.myapplication6.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.example.myapplication6.data.models.CityNode
import com.example.myapplication6.data.models.WeatherPersistence
import com.example.myapplication6.util.WeatherUtils.Companion.getActualWeather6HourlyItems
import com.example.myapplication6.util.WeatherUtils.Companion.toCel

class HourlyTemperatureGraphCanvasView(context: Context?, attrs: AttributeSet) : View(context,  attrs) {

    var cityNode: CityNode? = null
        set(value) {
            field = value
            Log.d(TAG, "set cityNode")
            invalidate()
        }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        Log.d(TAG, "onDraw()")
        if(cityNode==null){
            canvas!!.drawRect(
                    100F, 100F, 200F, 200F,
                    Paint().apply{color = Color.WHITE})
        }else {
            drawTemperatureIndicators(cityNode!!.weatherList!!, canvas)
        }
    }

    fun drawTemperatureIndicators(list: List<WeatherPersistence>, canvas: Canvas?){
        val paint = Paint().apply { color = Color.WHITE; textSize = 40F}
        val actualList = getActualWeather6HourlyItems(list).toList()
        val sortedTempList = actualList.map{it.temp}.sorted().toList()
        val minTemp = sortedTempList.first()
        val maxTemp = sortedTempList.last()

        val heightStep = (height-70)/(maxTemp-minTemp)
        val widthStep = width/12

        var x: Float = 0F
        var y: Float = 0F
        var prevX: Float = 0F
        var prevY: Float = 0F
        actualList.indices.forEach {
            x = calcPointX(it, widthStep)
            y = calcPointY(it, actualList[it].temp, heightStep, minTemp)
            canvas!!.drawText((actualList[it].tempMin!!).toString(), (x-30F),
                    y-20F, paint)

            canvas!!.drawCircle(x, y, 8F, paint)

            if(it!=0){
                val lineX = calcLineX(it, widthStep)
                canvas!!.drawLine(lineX, 0F, lineX, height.toFloat(), paint)
                canvas!!.drawLine(prevX, prevY, x, y, paint)
            }

            prevX = x
            prevY = y
        }
    }


    private fun calcPointX(ind: Int, widthStep: Int ): Float{
        return (widthStep*(2*ind+1)).toFloat()
    }

    private fun calcPointY(ind: Int, temp: Double, heightStep: Double, minTemp: Double): Float{
        return (height - (heightStep * (temp - minTemp))).toFloat()
    }

    private fun calcLineX(ind: Int, widthStep: Int ): Float{
        return (widthStep*(2*ind)).toFloat()
    }

//    fun getHourlyGraphCoordinatesArray(list: List<WeatherPersistence>?, viewWith: Int, viewHeight: Int) : FloatArray?{
//        //todo move to separate sequence method
//        Log.d(TAG,"aChub viewHeight: ${viewHeight}")
//        val sortedTempList = getActualWeather6HourlyItems(list!!).map{it.temp}.sorted().toList()
//        val minTemp = sortedTempList.first()
//        val maxTemp = sortedTempList.last()
//        Log.d(TAG,"aChub maxTemp: ${maxTemp}; minTemp : ${minTemp}")
//        val tempDPHeightStep = (viewHeight)/(toCel(maxTemp) - toCel(minTemp))
//        Log.d(TAG,"aChub tempDPHeightStep: ${tempDPHeightStep}")
//
//        val delim = viewWith/12
//        val resultArr = list?.let{
//            val rawList = WeatherUtils.getActualWeather6HourlyItems(it)
//            (rawList.indices).map{ num->listOf((delim*if(num!=0||num!=rawList.size-1){num*2+1}else{num}).toFloat(), fahrToCel(list[num].temp, tempDPHeightStep, minTemp, maxTemp),
//                    (delim*if(num!=0||num!=rawList.size-1){num*2+1}else{num}).toFloat(), fahrToCel(list[num].temp, tempDPHeightStep, minTemp, maxTemp))}
//                    .flatten()
//                    .dropLast(2)
//                    .drop(2)
//                    .toFloatArray()
//        }
//        Log.d(WeatherUtils.TAG, "getHourlyGraphCoordinatesArray() resultArr : ${resultArr?.joinToString { it.toString() }}")
//        return resultArr
//    }

    //todo move coefficients to parameters
    private fun fahrToCel(fahrenheit: Double, tempDPHeightStep: Double, minTemp: Double, maxTemp: Double): Float {
        val result = (((maxTemp - fahrenheit)*5/9)*tempDPHeightStep).toFloat()
        Log.d(TAG, "aChub fahrToCe() maxTemp - fahrenheit : ${maxTemp - fahrenheit}")
        Log.d(TAG, "aChub fahrToCe() result : ${result}")
        return result
    }


    companion object{
        val TAG = HourlyTemperatureGraphCanvasView::class.java.simpleName
    }
}