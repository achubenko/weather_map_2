package com.example.myapplication6.workers

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.myapplication6.data.models.CityAsset
import com.example.myapplication6.data.models.CityPersistance
import com.google.gson.Gson
import android.util.Log
import com.example.myapplication6.data.database.CitiesDao
import com.example.myapplication6.data.models.City
import com.example.myapplication6.util.JsonUtil.Companion.BATCH_SIZE
import com.example.myapplication6.util.JsonUtil.Companion.getCitiesParallelStreamFromFile
import com.google.gson.stream.JsonReader
import java.io.*
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.collections.ArrayList

class CityListPopulateDbWorker constructor(val context: Context, val params: WorkerParameters ): Worker(context, params){

    lateinit var citiesDao: CitiesDao

    override fun doWork(): Result {
        Log.d(TAG, "aChub doWork() citiesDao==null : ${citiesDao==null}")
        //todo replace with RX
        if(citiesDao.allList().isEmpty()) {
            parseFileWIthJson()
        }
        return Result.success()
    }

    //https://sites.google.com/site/gson/streaming
    fun parseFileWIthJson(){
        val cityList = ArrayList<CityPersistance>(BATCH_SIZE)
        val cityListFromJson = getCitiesParallelStreamFromFile(File(applicationContext.filesDir, "city.list2.json"))
        cityListFromJson.forEach{ city -> collectCitiesPiece(city, cityList)}
        if(cityList.size!=0){
            populateByPiece(cityList)
        }
    }

    private fun collectCitiesPiece(city: City,  cpArrayList: ArrayList<CityPersistance>){
        cpArrayList.add(city.toCityPersistance()!!)
        if(cpArrayList.size==BATCH_SIZE){
            populateByPiece(cpArrayList)
        }
    }

    private fun populateByPiece(cpArrayList: ArrayList<CityPersistance>){
        citiesDao.insertList(cpArrayList)
        cpArrayList.clear()
    }

    companion object{
        val TAG = CityListPopulateDbWorker::class.java.simpleName
    }

}

