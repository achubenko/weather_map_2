package com.example.myapplication6.workers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData



class LiveDataHelper {
    companion object{
        private var liveDataHelper: LiveDataHelper? = null
        @Synchronized
        fun getInstance(): LiveDataHelper? {
            if (liveDataHelper == null)
                liveDataHelper = LiveDataHelper()
            return liveDataHelper
        }

    }
//    private var liveDataHelper: LiveDataHelper? = null
    private val downloadPercent = MediatorLiveData<Int>()



    fun updateDownloadPer(percentage: Int) {
        downloadPercent.postValue(percentage)
    }

    fun observePercentage(): LiveData<Int> {
        return downloadPercent
    }
}