package com.example.myapplication6.workers

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.firebase.storage.FirebaseStorage

import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.net.URL
import java.nio.channels.Channels

class DownloadJsonFileWorker(val context: Context, params: WorkerParameters): Worker(context, params) {
    //    https://drive.google.com/file/d/17_aDXUBcNAh1MBo4wofMAhlvrRAQ27Ky/view?usp=sharing
    lateinit var liveDataHelper: LiveDataHelper

    val manager = (context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager)

    val receiver_complete = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent!!.getAction()
            if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                val extras = intent.extras
                val q = DownloadManager.Query()
                q.setFilterById(extras.getLong(DownloadManager.EXTRA_DOWNLOAD_ID));
                var c = manager.query(q);

                if (c.moveToFirst()) {
                    var status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    if (status == DownloadManager.STATUS_SUCCESSFUL) {

                        // get other required data by changing the constant passed to getColumnIndex

                }
            }
        }}
    }
////        override onReceive(context: Context,intent: Intent) {
//            val action = intent.getAction();
//            if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)){
//                Bundle extras = intent.getExtras();
//                DownloadManager.Query q = new DownloadManager.Query();
//                q.setFilterById(extras.getLong(DownloadManager.EXTRA_DOWNLOAD_ID));
//                Cursor c = manager.query(q);
//
//                if (c.moveToFirst()) {
//                    int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
//                    if (status == DownloadManager.STATUS_SUCCESSFUL) {
//                        // process download
//                        if(c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE)).equalsIgnoreCase("View"))
//                            viewPdf();
//                        else if(c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE)).equalsIgnoreCase("Share"))
//                            shareFile();
//                        // get other required data by changing the constant passed to getColumnIndex
//                    }else{
//                        Toast.makeText(context, R.string.error_creating_pdf_file,
//                                Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        }
//    };

    fun downloadFile() {
    val file = File(applicationContext.filesDir, "city.list2.json")
    val downloadRequest =
            DownloadManager.Request(Uri.parse("https://drive.google.com/file/d/17_aDXUBcNAh1MBo4wofMAhlvrRAQ27Ky/view?usp=sharing"))
                    .setTitle("Dummy File")// Title of the Download Notification
                    .setDescription("Downloading")// Description of the Download Notification
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)// Visibility of the download Notification
                    .setDestinationUri(Uri.fromFile(file))// Uri of the destination file
                    .setRequiresCharging(false)// Set if charging is required to begin the download
                    .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                    .setAllowedOverRoaming(true)// Set if download is allowed on roaming network

    val downloadId = manager.enqueue(downloadRequest)
        context!!.registerReceiver(receiver_complete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
}

    private fun downloadWithIO(): Result{
        val outputFile = File(applicationContext.filesDir, "city.list2.json")
        val url = URL("https://drive.google.com/file/d/17_aDXUBcNAh1MBo4wofMAhlvrRAQ27Ky/view?usp=sharing")
        val urlConnection = url.openConnection()
        urlConnection.connect()
        try{
            var fileLength = urlConnection.getContentLength()
            val fos = FileOutputStream(outputFile)
            val inputStream = urlConnection!!.getInputStream()
            val buffer = ByteArray(64)
            var len1: Int = inputStream.read(buffer)
            var  total = 0L
            while ((len1)>0){
                total += len1;
                var percentage = ((total * 100)/ fileLength).toInt()
//            liveDataHelper.updateDownloadPer(percentage)
                fos.write(buffer, 0, len1)
                len1 = inputStream.read(buffer)
            }
            fos.close()
            inputStream.close()
        }catch (e: Exception){
            e.printStackTrace()
            Log.d(TAG, "aChub Result.failure()")
            return Result.failure()
        }
        Log.d(TAG, "aChub Result.success()")
        return Result.success()
    }

//    private fun getDriveService(): Drive {
//        val applicationName = "Drive API Java Quickstart";
//		val clientSecretInput = DownloadJsonFileWorker::class.java.getResourceAsStream("/google/drive/example/v3/client_secret.json");
//		val localStoreDir = File(System.getProperty("user.home"), ".credentials/drive-java-quickstart.json")
//		val scopes = Arrays.asList(DriveScopes.DRIVE, DriveScopes.DRIVE_APPDATA, DriveScopes.DRIVE_FILE)
//		val connector = ClientSecretGoogleDriveConnector(applicationName, clientSecretInput, localStoreDir, scopes)
//		return connector.getDriveService()
//    }

    fun downloadWithNio(): Result{
//        val driveService = getDriveService()
        try {
            val url = URL("https://firebasestorage.googleapis.com/v0/b/my-project6-1555068603328.appspot.com/o/city.list.json?alt=media&token=05bbc7a6-9ea3-4f8e-8e8d-222d17ac0d6c")
            val outputFile = File(applicationContext.filesDir, "city.list2.json")
//            val fis = FileInputStream(outputFile)
            val readableByteChannel = Channels.newChannel(url.openStream())
            val fos = FileOutputStream(outputFile)
//            val fileChannel = fos.channel
            fos.channel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE)
        }catch (e: Exception){
                e.printStackTrace()
                Log.d(TAG, "aChub Result.failure()")
                return Result.failure()
            }
            Log.d(TAG, "aChub Result.success()")
            return Result.success()
    }

    private fun downloadFileFromFirebase(): Result{
        val storage = FirebaseStorage.getInstance()
        //todo move url to gradle properties
        val storRef = storage.getReferenceFromUrl("gs://my-project6-1555068603328.appspot.com/city.list.json")

//        val localFile = File(applicationContext.filesDir, "city.list2.json")
        val localFile = File.createTempFile("city.list2", "json")

        storRef.getFile(localFile)
        return Result.success()
    }

    override fun doWork(): Result {
        Log.d("DownloadJsonFileWorker", "aChub doWork()")
        return  downloadWithNio()
    }

    companion object{
        val TAG = DownloadJsonFileWorker::class.java.simpleName
    }

}