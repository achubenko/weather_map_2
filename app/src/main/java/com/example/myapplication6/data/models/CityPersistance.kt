package com.example.myapplication6.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "cities")
data class CityPersistance(@PrimaryKey var id: Long, var name: String, var country: String,
                           var population: Int, var longitude: Double, var latitude: Double,
                           var isTracked: Boolean = false): Serializable