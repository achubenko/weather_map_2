package com.example.myapplication6.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CityWeatherResponse {

    @Expose
    var list: Array<WeatherItem>? = null

    @Expose
    var city: City? = null

    inner class WeatherItem {
        @Expose
        var main: Main? = null

        @Expose
        var weather: Array<Weather>? = null

        @Expose
        var clouds: Clouds? = null

        @Expose
        var wind: Wind? = null

        @Expose
        var snow: Snow? = null

        @SerializedName("dt")
        var dateLong: Long? = null


    }

    inner class Main {

        @SerializedName("temp")
        var temp: Double? = null

        @SerializedName("temp_min")
        var tempMin: Double? = null

        @SerializedName("grnd_level")
        var grndLevel: Double? = null

        @SerializedName("humidity")
        var humidity: Int? = null

        @SerializedName("pressure")
        var pressure: Double? = null

        @SerializedName("sea_level")
        var seaLevel: Double? = null

        @SerializedName("temp_max")
        var tempMax: Double? = null
    }

    inner class Weather {
        @SerializedName("id")
        var id: Int = 0

        @SerializedName("main")
        var main: String? = null

        @SerializedName("description")
        var description: String? = null

        @SerializedName("icon")
        var icon: String? = null
    }

    inner class Clouds {

        @SerializedName("all")
        var all: Int? = null
    }

    inner class Wind {

        @SerializedName("speed")
        var speed: Double? = null

        @SerializedName("deg")
        var deg: Double? = null

    }

    inner class Snow {

        @SerializedName("3h")
        var parameter3h: Double? = null
    }

    inner class Sys {

        @SerializedName("country")
        var country: String? = null

        @SerializedName("sunrise")
        var sunrise: Int? = null

        @SerializedName("sunset")
        var sunset: Int? = null

        @SerializedName("message")
        var message: Double? = null
    }

    fun toWeatherPersistanceList() = if(city?.id==null){ null } else {
        list?.let { it.filter { w -> !(w.dateLong==null && w.main?.temp==null) }
            }?.map { toWeatherPersistance(it) }?.toList()
    }

    fun toWeatherPersistance(item: WeatherItem)=
            WeatherPersistence(
                    cityId = city!!.id!!, date = item.dateLong!!, temp = item.main!!.temp!!,
                    tempMin = item.main?.tempMin, grndLevel = item.main?.grndLevel,
                    humidity = item.main?.humidity, pressure = item.main?.pressure,
                    seaLevel = item.main?.seaLevel, tempMax = item.main?.tempMax,

                    icon = item.weather?.let{it[0].icon}, description = item.weather?.let{it[0].description},
                    mainWeather = item.weather?.let{it[0].main}, cloudity = item.clouds?.all,
                    windDeg = item.wind?.deg, windSpeed = item.wind?.speed,
                    snomTime = item.snow?.parameter3h
                    )

    companion object{
        val TAG = CityWeatherResponse::class.java.simpleName
    }
}

