package com.example.myapplication6.data.database

import androidx.room.*
import com.example.myapplication6.data.models.WeatherPersistence
import io.reactivex.Maybe

@Dao
interface WeatherDao {

    @Query("SELECT * FROM weather WHERE cityId = :cityId")
    fun getByCity(cityId: Long): Maybe<List<WeatherPersistence>>

    @Query("DELETE FROM weather WHERE cityId = :cityId")
    fun deleteByCity(cityId: Long): Int

    @Insert
    fun insert(items: List<WeatherPersistence>)

    @Transaction
    fun updateForCity(items: List<WeatherPersistence>, cityId: Long = items[0].cityId){
        deleteByCity(cityId)
        insert(items)
    }

}
