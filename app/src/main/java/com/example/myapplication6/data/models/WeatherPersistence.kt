package com.example.myapplication6.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "weather")
data class WeatherPersistence(
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,

        var cityId: Long,

        var date: Long,

        var temp: Double,

        var tempMin: Double?,
        var grndLevel: Double?,
        var humidity: Int?,
        var pressure: Double?,
        var seaLevel: Double?,
        var tempMax: Double?,

        var icon: String?,
        var description: String?,
        var mainWeather: String?,

        var cloudity: Int?,

        var windDeg: Double?,
        var windSpeed: Double?,

        var snomTime: Double?
): Serializable