package com.example.myapplication6.data.repository

import android.util.Log
import com.example.myapplication6.data.database.CitiesDao

import javax.inject.Inject
import javax.inject.Singleton

import com.example.myapplication6.data.models.CityPersistance

@Singleton
class CitiesRepository @Inject constructor(private val mCitiesDao: CitiesDao) {

    fun getAllCities() = mCitiesDao.all()

    fun getCitiesByCountry(country : String) = mCitiesDao.getByCountry(country)

    fun updateCity(city: CityPersistance) {
        //todo think about observing this
        mCitiesDao.updateCity(city)
    }

    fun getTrackedCities() = mCitiesDao.getTrackedCities()

    fun getCitiesByName(name: String) = mCitiesDao.get(maskName(name))

    companion object {
        private val TAG = CitiesRepository::class.java.simpleName
        private fun maskName(name: String) = "%"+name+"%"
    }
}
