package com.example.myapplication6.data.repository

import android.util.Log
import com.example.myapplication6.client.APIRequester
import com.example.myapplication6.data.database.WeatherDao

import javax.inject.Inject
import javax.inject.Singleton

import androidx.lifecycle.MutableLiveData
import com.example.myapplication6.data.models.CityNode
import com.example.myapplication6.data.models.CityPersistance
import com.example.myapplication6.data.models.CityWeatherResponse
import com.example.myapplication6.data.models.WeatherPersistence
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.*


@Singleton
class WeatherRepository @Inject

constructor(private val apiRequester: APIRequester, private val weatherDao: WeatherDao) {

    companion object {
        private val TAG = WeatherRepository::class.java.simpleName
    }

    val weatherListLiveData: MutableLiveData<List<WeatherPersistence>> = MutableLiveData()
    val errorMutableLiveData: MutableLiveData<Error> = MutableLiveData()

    fun getObservableCityNodeList(trackedCities: List<CityPersistance>):Observable<List<CityNode>>{

        
        Log.d(TAG, "aChub getObservableCityNodeList() trackedCities.size ${trackedCities.size}")
        var cityNodeList: MutableList<CityNode> = ArrayList<CityNode>()
        var observableCityNodeList: BehaviorRelay<List<CityNode>> = BehaviorRelay.create()
        trackedCities.asSequence()
                .forEach{city->Observable
                        .concat(
                                weatherDao.getByCity(city.id).toObservable(),   //room is already async
                                getNetworkCityWeather(city.id).toObservable()
                                        .doOnNext {
                                            Log.d(TAG, "aChub netNetworkCityWeather it: ${it.size}")
                                            weatherDao.updateForCity(cityId = city.id, items = it)
                                        }
                        )
                        .filter { !it.isNullOrEmpty() }
                        .doOnNext{
                            //todo optimize adding, add only the newest
                            Log.d(TAG, "aChub netNetworkCityWeather doOnNext list<WeatherPersistence> size: ${it.size}")
                            cityNodeList.add(CityNode(city, weatherList = it))
                            Log.d(TAG, "aChub netNetworkCityWeather cityNodeList.size: ${cityNodeList.size}")
                        }
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.newThread()).firstElement()
                        .subscribe({observableCityNodeList.accept(cityNodeList)},{ e->Log.d(TAG, e.message)})
                }

        return observableCityNodeList
    }



    //todo where should this method be?
    private fun getNetworkCityWeather(cityId: Long): Single<List<WeatherPersistence>> {
        Log.d(TAG, "aChub getNetworkCityWeather() invoked")
        return apiRequester.getWeather5d3hForCity(cityId)
                .map{ it.toWeatherPersistanceList() }
//        //todo think about handling errors
//                .doOnError { errorMutableLiveData.value = Error("Error Fetching Data!")}
    }

    private fun getNetworkCityWeather2(cityId: Long): Single<CityWeatherResponse> {
        Log.d(TAG, "aChub getNetworkCityWeather() invoked")
        return apiRequester.getWeather5d3hForCity(cityId)
    }
}
