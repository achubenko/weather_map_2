package com.example.myapplication6.data.database

import com.example.myapplication6.data.repository.WeatherRepository
import com.example.myapplication6.data.repository.CitiesRepository

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import com.example.myapplication6.data.models.CityPersistance
import com.example.myapplication6.data.models.WeatherPersistence
import com.example.myapplication6.workers.CityListPopulateDbWorker


@Database(version = 3, exportSchema = false, entities = [CityPersistance::class, WeatherPersistence::class])
abstract class AppDatabase : RoomDatabase() {

    abstract fun citiesDao(): CitiesDao

    abstract fun movieDetailsDao(): WeatherDao

    companion object {
        private val TAG = AppDatabase::class.java.simpleName
        val NAME = "app_database.db"
    }
}
