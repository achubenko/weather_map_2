package com.example.myapplication6.data.models

import android.util.Log
import com.google.gson.annotations.SerializedName

class City {

    init{
//        Log.d("City", "init")
    }

    @SerializedName("id")
    var id: Long? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("country")
    var country: String = ""

    @SerializedName("population")
    var population: Int = 0

    @SerializedName("coordinates", alternate= ["coord"])
    var coordinates: Coordinates? = null

    inner class Coordinates{
        @SerializedName("lon")
        var lon: Double? = null

        @SerializedName("lat")
        var lat: Double? = null
    }

    fun toCityPersistance(): CityPersistance?{
        return if(id ==null|| name ==null|| coordinates?.lon ==null|| coordinates?.lat == null){ null }
            else { CityPersistance(id=id!!, name=name!!, country = country, population = population,
                    longitude = coordinates!!.lon!!, latitude = coordinates!!.lat!!)
        }
    }
}