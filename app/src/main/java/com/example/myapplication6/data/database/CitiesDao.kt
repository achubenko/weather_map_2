package com.example.myapplication6.data.database

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import com.example.myapplication6.data.models.CityPersistance
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface CitiesDao {

    //todo think about Flowable

    @Query("SELECT * FROM cities")
    fun all(): Flowable<List<CityPersistance>>

    @Query("SELECT * FROM cities WHERE name LIKE :name")
    operator fun get(name: String): Single<List<CityPersistance>>

    @Query("SELECT * FROM cities WHERE country LIKE :country")
    fun getByCountry(country: String): Flowable<List<CityPersistance>>

    @Query("SELECT * FROM cities WHERE isTracked = :isTracked")
    fun getTrackedCities(isTracked: Boolean = true): Flowable<List<CityPersistance>>

    @Insert
    fun insertList(cities: List<CityPersistance>)

    @Update
    fun updateCity(city: CityPersistance)

    @RawQuery
    fun rawQuery(query: SupportSQLiteQuery): Boolean

    @Query("SELECT * FROM cities")
    fun allList(): List<CityPersistance>
}
