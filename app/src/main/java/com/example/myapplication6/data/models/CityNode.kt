package com.example.myapplication6.data.models

import java.io.Serializable

class CityNode(val city: CityPersistance, val weatherList: List<WeatherPersistence>?): Serializable