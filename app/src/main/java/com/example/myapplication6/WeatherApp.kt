package com.example.myapplication6


import android.app.Activity
import android.app.Application
import androidx.work.*
import com.example.myapplication6.data.database.CitiesDao
import com.example.myapplication6.di.MyWorkerFactory
import com.example.myapplication6.di.components.AppComponent

import com.example.myapplication6.di.components.DaggerAppComponent
import com.example.myapplication6.workers.CityListPopulateDbWorker
import com.example.myapplication6.workers.DeleteJsonFileDbWorker
import com.example.myapplication6.workers.DownloadJsonFileWorker

import javax.inject.Inject

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import java.util.concurrent.Executors

class WeatherApp : DaggerApplication(), HasActivityInjector {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {

        val appComponent = DaggerAppComponent.builder()
                .application(this)
                .build()
        appComponent.inject(this)

        return appComponent
    }

    @set:Inject
    internal var activityDispatchingInjector: DispatchingAndroidInjector<Activity>? = null

    @Inject
    lateinit var myWorkerFactory: WorkerFactory

    @Inject
    lateinit var citiesDao: CitiesDao

    override fun onCreate() {
        super.onCreate()
//        initializeComponent()
        setInstance(this)
        if(citiesDao.allList().size<200_000)initWorkers()
    }


    private  fun initWorkers(){
        WorkManager.initialize(this, Configuration.Builder().setWorkerFactory(myWorkerFactory).setExecutor(Executors.newFixedThreadPool(2)).build())
        val downloadJsonFileRequest = OneTimeWorkRequest.from(DownloadJsonFileWorker::class.java)
        val populateDbRequest: OneTimeWorkRequest = OneTimeWorkRequest.from(CityListPopulateDbWorker::class.java)
        val deleteJsonFileRequest: OneTimeWorkRequest = OneTimeWorkRequest.from(DeleteJsonFileDbWorker::class.java)
//        LiveDataHelper.getInstance()!!.observePercentage().observe(this, new Observer<Integer>() {
//            overide onChanged(Integer integer) {
//                progressBar.setProgress(integer);
//                progress_tv.setText(integer+"/100");
//            }

        val workManager = WorkManager.getInstance(applicationContext)

//        var continuation = workManager
//                .beginUniqueWork(
//                        IMAGE_MANIPULATION_WORK_NAME,
//                        ExistingWorkPolicy.REPLACE,
//                        OneTimeWorkRequest.from(CleanupWorker::class.java)
//                )

        workManager.beginWith(downloadJsonFileRequest).then(populateDbRequest).then(deleteJsonFileRequest).enqueue()




    }

//    private fun initializeComponent() {
//        val appComponent = DaggerAppComponent.builder()
//                .application(this)
//                .build()
//
//                appComponent.inject(this)
//
//    }

    override fun activityInjector(): DispatchingAndroidInjector<Activity>? {
        return activityDispatchingInjector
    }

    companion object {

        var appContext: WeatherApp? = null
            private set

        @Synchronized
        private fun setInstance(app: WeatherApp) {
            appContext = app
        }
    }
}
